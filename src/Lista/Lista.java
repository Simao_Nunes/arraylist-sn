/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author TPSI
 */
public class Lista {

    /**
     * @param args the command line arguments
     */
    static boolean adicionar = true;
    static Scanner leitor = new Scanner(System.in);
    static ArrayList<Integer> id = new ArrayList<>();
    static ArrayList<String> ilhas = new ArrayList<>();
    static int opcao;

    public static void main(String[] args) {

        inserir();
        menu();

    }

    private static void inserir() {

        while (adicionar == true) {

            System.out.println("Insira uma ilha: ");
            String item = leitor.nextLine();
            if (ilhas.contains(item)) {
                System.out.println("Ilha já existe!");
                menu();
            }
            System.out.println("Insira o ID da ilha: ");
            int idnt = leitor.nextInt();
            if (id.contains(idnt)) {
                System.out.println("ID da ilha já existe!");
                menu();
            }
            leitor.nextLine();
            ilhas.add(item);
            id.add(idnt);

            System.out.println("Adicionar mais? s/n");
            String sn = leitor.nextLine();

            if (sn.equals("s")) {
            } else if (sn.equals("n")) {
                adicionar = false;
            } else {
                System.out.println("Opção inválida!");
                adicionar = false;
            }
        }
        adicionar = true;
        System.out.println("____________________________");
    }

    private static void menu() {

        System.out.println("Opções:");
        System.out.println("1 -> Listar");
        System.out.println("2 -> Modificar");
        System.out.println("3 -> Procurar por Nome");
        System.out.println("4 -> Procurar por ID");
        System.out.println("0 -> Sair");
        System.out.println("____________________________");

        opcao = leitor.nextInt();
        leitor.nextLine();

        switch (opcao) {
            case 1:
                listar();
                menu();
                break;
            case 2:
                modificar();
                break;
            case 3:
                System.out.println("Insira o nome da ilha que procura:");
                String item = leitor.nextLine();
                if (ilhas.contains(item)) {
                    int pos = ilhas.indexOf(item);
                    System.out.println("A ilha " + ilhas.get(pos) + " tem o id " + id.get(pos));
                    menu();
                } else {
                    System.out.println("Ilha não encontrado!");
                    menu();
                }
                System.out.println("____________________________");
                break;
            case 4:
                System.out.println("Insira o ID da ilha que procura:");
                int idnt = leitor.nextInt();
                leitor.nextLine();
                if (id.contains(idnt)) {
                    int pos = id.indexOf(idnt);
                    System.out.println("A ilha " + ilhas.get(pos) + " tem o id " + id.get(pos));
                    menu();
                } else {
                    System.out.println("ID não encontrado!");
                    menu();
                }
                System.out.println("____________________________");
                break;
            case 0:
                System.out.println("Adês");
                System.exit(0);
                break;
            default:
                System.out.println("Opção inválida!");
                menu();
                break;
        }
    }

    private static void modificar() {
        System.out.println("Modificar:");
        System.out.println("1 -> Inserir");
        System.out.println("2 -> Alterar Nome");
        System.out.println("3 -> Eliminar Item");
        System.out.println("0 -> Back");
        System.out.println("____________________________");

        opcao = leitor.nextInt();
        leitor.nextLine();
        switch (opcao) {
            case 1:
                inserir();
                modificar();
                break;
            case 2:
                listar();
                System.out.println("Qual o ID da ilha que quer alterar?:");
                int idnt = leitor.nextInt();
                leitor.nextLine();
                if (id.contains(idnt)) {
                    int pos = id.indexOf(idnt);
                    System.out.println("Novo nome para a ilha " + idnt + ": ");
                    String newitem = leitor.nextLine();
                    ilhas.set(pos, newitem);
                    listar();
                    modificar();
                } else {
                    System.out.println("ID não encontrado!");
                    modificar();
                }
                break;
            case 3:
                listar();
                System.out.println("Qual a ilha que quer apagar?:");
                String item = leitor.nextLine();
                if (ilhas.contains(item)) {
                    int pos = ilhas.indexOf(item);
                    ilhas.remove(pos);
                    listar();
                    modificar();
                } else {
                    System.out.println("Ilha não encontrada!");
                    modificar();
                }
                break;
            case 0:
                menu();
                break;
            default:
                menu();
                break;
        }
    }

    private static void listar() {
        System.out.println("____________________________");
        System.out.println("Ilha | ID");
        for (int x = 0; x < ilhas.size(); x++) {
            System.out.println(ilhas.get(x) + " | " + id.get(x));
        }
        System.out.println("____________________________");
    }

}
